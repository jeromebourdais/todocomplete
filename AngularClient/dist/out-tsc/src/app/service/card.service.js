var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { MessageService } from '../message.service';
var httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
var CardService = /** @class */ (function () {
    function CardService(http, messageService) {
        this.http = http;
        this.messageService = messageService;
        this.cardUrl = 'http://localhost:5000/api/Card'; // URL to web api
    }
    /** GET cards from the server */
    CardService.prototype.getCards = function () {
        var _this = this;
        var url = this.cardUrl + "/full";
        console.log(url);
        return this.http.get(this.cardUrl)
            .pipe(map(function (res) { return res; }), tap(function (_) { return _this.log('fetched cards'); }), catchError(this.handleError('getcards', [])));
    };
    /** GET Card by id. Return `undefined` when id not found */
    CardService.prototype.getCardNo404 = function (id) {
        var _this = this;
        var url = this.cardUrl + "/?id=" + id;
        return this.http.get(url)
            .pipe(map(function (cards) { return cards[0]; }), // returns a {0|1} element array
        tap(function (h) {
            var outcome = h ? "fetched" : "did not find";
            _this.log(outcome + " card id=" + id);
        }), catchError(this.handleError("getcard id=" + id)));
    };
    /** GET card by id. Will 404 if id not found */
    CardService.prototype.getCard = function (id) {
        var _this = this;
        var url = this.cardUrl + "/" + id;
        return this.http.get(url).pipe(tap(function (_) { return _this.log("fetched card id=" + id); }), catchError(this.handleError("getcard id=" + id)));
    };
    /* GET cards whose name contains search term */
    CardService.prototype.searchCards = function (term) {
        var _this = this;
        if (!term.trim()) {
            // if not search term, return empty card array.
            return of([]);
        }
        return this.http.get(this.cardUrl + "/?name=" + term).pipe(tap(function (_) { return _this.log("found cards matching \"" + term + "\""); }), catchError(this.handleError('searchcards', [])));
    };
    //////// Save methods //////////
    /** POST: add a new card to the server */
    CardService.prototype.addCard = function (card) {
        var _this = this;
        return this.http.post(this.cardUrl, card, httpOptions).pipe(tap(function (newcard) { return _this.log("added card w/ id=" + newcard.id); }), catchError(this.handleError('addcard')));
    };
    /** DELETE: delete the card from the server */
    CardService.prototype.deleteCard = function (card) {
        var _this = this;
        var id = typeof card === 'number' ? card : card.id;
        var url = this.cardUrl + "/" + id;
        return this.http.delete(url, httpOptions).pipe(tap(function (_) { return _this.log("deleted card id=" + id); }), catchError(this.handleError('deletecard')));
    };
    /** PUT: update the card on the server */
    CardService.prototype.updateCard = function (card) {
        var _this = this;
        var url = this.cardUrl + "/" + card.id;
        return this.http.put(url, card, httpOptions).pipe(tap(function (_) { return _this.log("updated card id=" + card.id); }), catchError(this.handleError('updatecard')));
    };
    CardService.prototype.updateList = function (cards) {
        return __awaiter(this, void 0, void 0, function () {
            // Sqlite ?
            function delay(ms) {
                return new Promise(function (resolve) { return setTimeout(resolve, ms); });
            }
            var id;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        id = 1;
                        //Suppression
                        return [4 /*yield*/, cards.map(function (t) { return __awaiter(_this, void 0, void 0, function () {
                                return __generator(this, function (_a) {
                                    console.log('del' + t.id);
                                    this.deleteCard(t).toPromise();
                                    return [2 /*return*/];
                                });
                            }); })];
                    case 1:
                        //Suppression
                        _a.sent();
                        return [4 /*yield*/, delay(2000)];
                    case 2:
                        _a.sent();
                        //Recréation
                        return [4 /*yield*/, cards.map(function (t) { return __awaiter(_this, void 0, void 0, function () {
                                return __generator(this, function (_a) {
                                    t.id = id++;
                                    console.log('add ' + t.id);
                                    this.addCard(t).toPromise();
                                    return [2 /*return*/];
                                });
                            }); })];
                    case 3:
                        //Recréation
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    /**
     * Handle Http operation that failed.
     * Let the app continue.
     * @param operation - name of the operation that failed
     * @param result - optional value to return as the observable result
     */
    CardService.prototype.handleError = function (operation, result) {
        var _this = this;
        if (operation === void 0) { operation = 'operation'; }
        return function (error) {
            // card: send the error to remote logging infrastructure
            console.error(error); // log to console instead
            // card: better job of transforming error for user consumption
            _this.log(operation + " failed: " + error.message);
            // Let the app keep running by returning an empty result.
            return of(result);
        };
    };
    /** Log a HeroService message with the MessageService */
    CardService.prototype.log = function (message) {
        this.messageService.add("cardService: " + message);
    };
    CardService = __decorate([
        Injectable({ providedIn: 'root' }),
        __metadata("design:paramtypes", [HttpClient,
            MessageService])
    ], CardService);
    return CardService;
}());
export { CardService };
//# sourceMappingURL=card.service.js.map