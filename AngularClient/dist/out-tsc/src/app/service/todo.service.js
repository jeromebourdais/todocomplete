var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { MessageService } from '../message.service';
var httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
var TodoService = /** @class */ (function () {
    function TodoService(http, messageService) {
        this.http = http;
        this.messageService = messageService;
        this.todoUrl = 'http://localhost:5000/api/Todo'; // URL to web api
    }
    /** GET todos from the server */
    TodoService.prototype.getTodos = function () {
        var _this = this;
        return this.http.get(this.todoUrl)
            .pipe(tap(function (_) { return _this.log('fetched todos'); }), catchError(this.handleError('getodos', [])));
    };
    /** GET Todo by id. Return `undefined` when id not found */
    TodoService.prototype.getTodoNo404 = function (id) {
        var _this = this;
        var url = this.todoUrl + "/?id=" + id;
        return this.http.get(url)
            .pipe(map(function (todos) { return todos[0]; }), // returns a {0|1} element array
        tap(function (h) {
            var outcome = h ? "fetched" : "did not find";
            _this.log(outcome + " todo id=" + id);
        }), catchError(this.handleError("getTodo id=" + id)));
    };
    /** GET Todo by id. Will 404 if id not found */
    TodoService.prototype.getTodo = function (id) {
        var _this = this;
        var url = this.todoUrl + "/" + id;
        return this.http.get(url).pipe(tap(function (_) { return _this.log("fetched todo id=" + id); }), catchError(this.handleError("getTodo id=" + id)));
    };
    /* GET todos whose name contains search term */
    TodoService.prototype.searchTodos = function (term) {
        var _this = this;
        if (!term.trim()) {
            // if not search term, return empty todo array.
            return of([]);
        }
        return this.http.get(this.todoUrl + "/?name=" + term).pipe(tap(function (_) { return _this.log("found todos matching \"" + term + "\""); }), catchError(this.handleError('searchtodos', [])));
    };
    //////// Save methods //////////
    /** POST: add a new todo to the server */
    TodoService.prototype.addTodo = function (todo) {
        var _this = this;
        return this.http.post(this.todoUrl, todo, httpOptions).pipe(tap(function (newTodo) { return _this.log("added todo w/ id=" + newTodo.id); }), catchError(this.handleError('addTodo')));
    };
    /** DELETE: delete the todo from the server */
    TodoService.prototype.deleteTodo = function (todo) {
        var _this = this;
        var id = typeof todo === 'number' ? todo : todo.id;
        var url = this.todoUrl + "/" + id;
        return this.http.delete(url, httpOptions).pipe(tap(function (_) { return _this.log("deleted todo id=" + id); }), catchError(this.handleError('deleteTodo')));
    };
    /** PUT: update the todo on the server */
    TodoService.prototype.updateTodo = function (todo) {
        var _this = this;
        var url = this.todoUrl + "/" + todo.id;
        return this.http.put(url, todo, httpOptions).pipe(tap(function (_) { return _this.log("updated todo id=" + todo.id); }), catchError(this.handleError('updateTodo')));
    };
    TodoService.prototype.updateList = function (todos) {
        var _this = this;
        var url = this.todoUrl + "/Reorder";
        console.log(url);
        console.log(todos);
        console.log(httpOptions);
        return this.http.put(url, todos, httpOptions).pipe(tap(function (_) { return _this.log("reordered"); }), catchError(this.handleError('updateTodo')));
    };
    /**
     * Handle Http operation that failed.
     * Let the app continue.
     * @param operation - name of the operation that failed
     * @param result - optional value to return as the observable result
     */
    TodoService.prototype.handleError = function (operation, result) {
        var _this = this;
        if (operation === void 0) { operation = 'operation'; }
        return function (error) {
            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead
            // TODO: better job of transforming error for user consumption
            _this.log(operation + " failed: " + error.message);
            // Let the app keep running by returning an empty result.
            return of(result);
        };
    };
    /** Log a HeroService message with the MessageService */
    TodoService.prototype.log = function (message) {
        this.messageService.add("TodoService: " + message);
    };
    TodoService = __decorate([
        Injectable({ providedIn: 'root' }),
        __metadata("design:paramtypes", [HttpClient,
            MessageService])
    ], TodoService);
    return TodoService;
}());
export { TodoService };
//# sourceMappingURL=todo.service.js.map