var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Todo } from '../model/todo';
import { TodoService } from '../service/todo.service';
var todoDetailComponent = /** @class */ (function () {
    function todoDetailComponent(route, todoService, location) {
        this.route = route;
        this.todoService = todoService;
        this.location = location;
    }
    todoDetailComponent.prototype.ngOnInit = function () {
        this.getTodo();
    };
    todoDetailComponent.prototype.getTodo = function () {
        var _this = this;
        var id = +this.route.snapshot.paramMap.get('id');
        this.todoService.getTodo(id)
            .subscribe(function (todo) { return _this.todo = todo; });
    };
    todoDetailComponent.prototype.goBack = function () {
        this.location.back();
    };
    todoDetailComponent.prototype.save = function () {
        var _this = this;
        this.todoService.updateTodo(this.todo)
            .subscribe(function () { return _this.goBack(); });
    };
    __decorate([
        Input(),
        __metadata("design:type", Todo)
    ], todoDetailComponent.prototype, "todo", void 0);
    todoDetailComponent = __decorate([
        Component({
            selector: 'app-todo-detail',
            templateUrl: './todo-detail.component.html',
            styleUrls: ['./todo-detail.component.css']
        }),
        __metadata("design:paramtypes", [ActivatedRoute,
            TodoService,
            Location])
    ], todoDetailComponent);
    return todoDetailComponent;
}());
export { todoDetailComponent };
//# sourceMappingURL=todo-detail.component.js.map