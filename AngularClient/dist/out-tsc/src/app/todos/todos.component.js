var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { TodoService } from '../service/todo.service';
import { moveItemInArray } from '@angular/cdk/drag-drop';
import { CardService } from '../service/card.service';
var TodosComponent = /** @class */ (function () {
    function TodosComponent(todoService, cardService) {
        this.todoService = todoService;
        this.cardService = cardService;
    }
    TodosComponent.prototype.ngOnInit = function () {
        this.getCards();
        this.getTodos();
    };
    TodosComponent.prototype.getCards = function () {
        var _this = this;
        this.cardService.getCards()
            .subscribe(function (cards) {
            _this.cards = cards;
            console.log(cards);
        });
    };
    TodosComponent.prototype.getTodos = function () {
        var _this = this;
        this.todoService.getTodos()
            .subscribe(function (todos) {
            _this.todos = todos;
            // tri par ordre
            _this.todos.sort(function (a, b) { return a.ordre < b.ordre ? -1 : a.ordre > b.ordre ? 1 : 0; });
        });
    };
    TodosComponent.prototype.addTodo = function (description) {
        var _this = this;
        description = description.trim();
        if (!description) {
            return;
        }
        this.todoService.addTodo({ description: description })
            .subscribe(function (todo) {
            _this.todos.push(todo);
        });
    };
    TodosComponent.prototype.delete = function (todo) {
        this.todos = this.todos.filter(function (h) { return h !== todo; });
        this.todoService.deleteTodo(todo).subscribe();
    };
    TodosComponent.prototype.drop = function (event) {
        var _this = this;
        moveItemInArray(this.todos, event.previousIndex, event.currentIndex);
        // put => subscribe !
        this.todoService.updateList(this.todos)
            .subscribe(function () { return _this.getTodos(); });
    };
    TodosComponent = __decorate([
        Component({
            selector: 'app-todoes',
            templateUrl: './todos.component.html',
            styleUrls: ['./todos.component.css']
        }),
        __metadata("design:paramtypes", [TodoService,
            CardService])
    ], TodosComponent);
    return TodosComponent;
}());
export { TodosComponent };
//# sourceMappingURL=todos.component.js.map