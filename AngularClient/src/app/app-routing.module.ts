import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DashboardComponent }   from './hero-dashboard/dashboard.component';
import { HeroesComponent }      from './heroes/heroes.component';
import { HeroDetailComponent }  from './hero-detail/hero-detail.component';
import { TodosComponent } from './todos/todos.component';
import { todoDetailComponent } from './todo-detail/todo-detail.component';
import { TodosOfCardComponent } from './todosOfCard/todosOfCard.component';

const routes: Routes = [
  { path: '', redirectTo: '/hero-dashboard', pathMatch: 'full' },
  { path: 'hero-dashboard', component: DashboardComponent },
  { path: 'heroDetail/:id', component: HeroDetailComponent },
  { path: 'heroes', component: HeroesComponent },

  { path: 'todos', component: TodosComponent },
  { path: 'todoDetail/:id', component: todoDetailComponent },
  { path: 'todosOfCard/:id', component: TodosOfCardComponent },

];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
