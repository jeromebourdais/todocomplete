import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { MessageService } from '../message.service';
import { Card } from '../model/card';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({ providedIn: 'root' })
export class CardService {

  private cardUrl = 'http://localhost:5000/api/Card';  // URL to web api

  constructor(
    private http: HttpClient,
    private messageService: MessageService) { }




  /** GET card by id. Will 404 if id not found */
  getCard(id: number): Observable<Card> {
    const url = `${this.cardUrl}/${id}`;
    return this.http.get<Card>(url)
      .pipe(
        //map(r=>r),
        tap(_ => this.log(`fetched card id=${id} ${url}`)),
        catchError(this.handleError<Card>(`getcard id=${id}`))
      );
  }

  /** GET cards from the server */
  getCards() {
    const url = `${this.cardUrl}/Full`;
    return this.http.get<Card[]>(url)
      .pipe(
        map(res => res),
        tap(_ => this.log('fetched cards'))
        //catchError(this.handleError<Card[]>('getcards', []))
      );
  }

  test() {
    const url = `${this.cardUrl}/Full`;
    return this.http.get<Card[]>(url)
      .pipe(
        map(res => res),
        tap(_ => this.log('fetched cards'))
        //catchError(this.handleError<Card[]>('getcards', []))
      );
  }

  /** GET Card by id. Return `undefined` when id not found */
  getCardNo404<Data>(id: number): Observable<Card> {
    const url = `${this.cardUrl}/?id=${id}`;
    return this.http.get<Card[]>(url)
      .pipe(
        map(cards => cards[0]), // returns a {0|1} element array
        tap(h => {
          const outcome = h ? `fetched` : `did not find`;
          this.log(`${outcome} card id=${id}`);
        }),
        catchError(this.handleError<Card>(`getcard id=${id}`))
      );
  }



  /* GET cards whose name contains search term */
  searchCards(term: string): Observable<Card[]> {
    if (!term.trim()) {
      // if not search term, return empty card array.
      return of([]);
    }
    return this.http.get<Card[]>(`${this.cardUrl}/?name=${term}`).pipe(
      tap(_ => this.log(`found cards matching "${term}"`)),
      catchError(this.handleError<Card[]>('searchcards', []))
    );
  }

  //////// Save methods //////////

  /** POST: add a new card to the server */
  addCard(card: Card): Observable<Card> {
    return this.http.post<Card>(this.cardUrl, card, httpOptions).pipe(
      tap((newcard: Card) => this.log(`added card w/ id=${newcard.id}`)),
      catchError(this.handleError<Card>('addcard'))
    );
  }

  /** DELETE: delete the card from the server */
  deleteCard(card: Card | number): Observable<Card> {
    const id = typeof card === 'number' ? card : card.id;
    const url = `${this.cardUrl}/${id}`;

    return this.http.delete<Card>(url, httpOptions).pipe(
      tap(_ => this.log(`deleted card id=${id}`)),
      catchError(this.handleError<Card>('deletecard'))
    );
  }

  /** PUT: update the card on the server */
  updateCard(card: Card): Observable<any> {
    const url = `${this.cardUrl}/${card.id}`;
    return this.http.put(url, card, httpOptions).pipe(
      tap(_ => this.log(`updated card id=${card.id}`)),
      catchError(this.handleError<any>('updatecard'))
    );
  }


  async updateList(cards: Card[]) {
    // réaffecter les id
    let id = 1;

    //Suppression
    await cards.map(async t => {
      console.log('del' + t.id);
      this.deleteCard(t).toPromise();
    });

    // Sqlite ?
    function delay(ms: number) {
      return new Promise(resolve => setTimeout(resolve, ms));
    }

    await delay(2000);

    //Recréation
    await cards.map(async t => {
      t.id = id++;
      console.log('add ' + t.id);
      this.addCard(t).toPromise();
    })



  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // card: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // card: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /** Log a HeroService message with the MessageService */
  private log(message: string) {
    this.messageService.add(`cardService: ${message}`);
  }
}
