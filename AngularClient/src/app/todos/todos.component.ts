import { Component, OnInit } from '@angular/core';
import { Todo } from '../model/todo';
import { TodoService } from '../service/todo.service';

import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { CardService } from '../service/card.service';

import { Card } from '../model/card';

@Component({
  selector: 'app-todoes',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css']
})
export class TodosComponent implements OnInit {
  todos: Todo[];
  cards: Card[];
  card: Card;


  constructor(
    private todoService: TodoService,
    private cardService: CardService
  ) { }

  ngOnInit() {
    this.getTodos();
    this.getCards();
  }

  getCard(id: number): void {
    this.cardService.getCard(id)
      .subscribe(
        card => {
          this.card = card as Card,
            console.log(`getCard`);
          console.log(this.card);
          console.log(this.card.todos[0].description);
        }
      );
  }

  getCards(): void {
    this.cardService.getCards().subscribe(r => { this.cards = r });
  }

  getTodos(): void {
    this.todoService.getTodos()
      .subscribe(todos => {
        this.todos = todos;
        // tri par ordre
        this.todos.sort((a, b) => a.ordre < b.ordre ? -1 : a.ordre > b.ordre ? 1 : 0)
      });
  }

  addTodo(description: string): void {
    description = description.trim();
    if (!description) { return; }
    this.todoService.addTodo({ description } as Todo)
      .subscribe(todo => {
        this.todos.push(todo);
      });
  }

  delete(todo: Todo): void {
    this.todos = this.todos.filter(h => h !== todo);
    this.todoService.deleteTodo(todo).subscribe();
  }

  drop(event: CdkDragDrop<Todo[]>) {
    moveItemInArray(this.todos, event.previousIndex, event.currentIndex);

    // put => subscribe !
    this.todoService.updateList(this.todos)
      .subscribe(
        () => this.getTodos()
      );
  }



}
