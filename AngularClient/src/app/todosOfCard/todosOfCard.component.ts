import { Component, OnInit, Input } from '@angular/core';
import { Todo } from '../model/todo';
import { TodoService } from '../service/todo.service';

import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { CardService } from '../service/card.service';

import { Card } from '../model/card';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-todosOfCard',
  templateUrl: './todosOfCard.component.html',
  styleUrls: ['./todosOfCard.component.css']
})
export class TodosOfCardComponent implements OnInit {
  @Input() card: Card;
  todos: Todo[];

  constructor(
    private route: ActivatedRoute,
    private todoService: TodoService,
    private cardService: CardService
  ) {

  }

  ngOnInit() {
    this.getCard();
  }

  getCard(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.cardService.getCard(id)
      .subscribe(
        card => {
          this.card = card as Card,
          console.log(this.card);
          this.todos = this.card.todos;
          this.todos.sort((a, b) => a.ordre < b.ordre ? -1 : a.ordre > b.ordre ? 1 : 0)
        }
      );
  }


  getTodos(): void {
    this.todoService.getTodos()
      .subscribe(todos => {
        this.todos = todos;
        // tri par ordre
        this.todos.sort((a, b) => a.ordre < b.ordre ? -1 : a.ordre > b.ordre ? 1 : 0)
      });
  }

  addTodo(description: string): void {
    description = description.trim();
    if (!description) { return; }
    this.todoService.addTodoToCard(this.card.id,{ description } as Todo)
      .subscribe(todo => {
        this.todos.push(todo);
      });
  }

  delete(todo: Todo): void {
    this.todos = this.todos.filter(h => h !== todo);
    this.todoService.deleteTodo(todo).subscribe();
  }

  drop(event: CdkDragDrop<Todo[]>) {
    moveItemInArray(this.todos, event.previousIndex, event.currentIndex);

    // put => subscribe !
    this.todoService.updateList(this.todos)
      .subscribe(
        () => this.getCard()
      );
  }



}
