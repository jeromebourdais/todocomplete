﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HomeBackend.Data;
using HomeBackend.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace HomeBackend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CardController : ControllerBase
    {
        private readonly TodoContext _context;



        public CardController(TodoContext context)
        {
            _context = context;
        }

        // GET: api/Cards
        [HttpGet]
        public IEnumerable<Card> GetCard()
        {
            return _context.Cards;
        }

        // GET: api/Cards
        [HttpGet("Full")]
        public IEnumerable<Card> GetCardFull()
        {
            var cards = _context.Cards;
            foreach(var c in cards)
            {
                c.todos = _context.Todos.Where(t => t.CardId == c.Id).ToList();
            }
            return cards;
        }

        // GET: api/Card/5
        [HttpGet("{id}", Name = "GetCard")]
        public Card Get(int id)
        {
            var card = _context.Cards.FirstOrDefault(h => h.Id == id);

            card.todos = _context.Todos.Where(t => t.CardId == card.Id).ToList();


            return card;
        }

        // PUT: api/Card/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCard([FromRoute] int id, [FromBody] Card card)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != card.Id)
            {
                return BadRequest();
            }

            _context.Entry(card).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!HeroExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        //// PUT: api/Card/5
        //[HttpPut("all")]
        //public async Task<IActionResult> PutCards([FromBody] Card[] todos)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    if (id != todo.Id)
        //    {
        //        return BadRequest();
        //    }

        //    _context.Entry(todo).State = EntityState.Modified;

        //    try
        //    {
        //        await _context.SaveChangesAsync();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!HeroExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return NoContent();
        //}


        // POST: api/Card
        [HttpPost]
        public async Task<IActionResult> PostCard([FromBody] Card todo)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Cards.Add(todo);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCard", new { id = todo.Id }, todo);
        }

        // DELETE: api/Card/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCard([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var todo = await _context.Cards.FindAsync(id);
            if (todo == null)
            {
                return NotFound();
            }

            _context.Cards.Remove(todo);
            await _context.SaveChangesAsync();

            return Ok(todo);
        }

        private bool HeroExists(int id)
        {
            return _context.Heroes.Any(e => e.Id == id);
        }
    }
}
