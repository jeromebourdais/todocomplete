﻿using HomeBackend.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HomeBackend.Mock
{
    public class MockDataSource
    {
        public IList<Hero> Heroes { get; private set; } = new List<Hero>
        {
            new Hero
            {
                Id = 10,
                Name = "John"
            },
            new Hero
            {
                Id = 11,
                Name = "Doe"
            }
        };

        public IList<Todo> Todos { get; private set; } = new List<Todo>
        {
            new Todo
            {
                Id = 10,
                Description = "Super important"
            },
            new Todo
            {
                Id = 11,
                Description = "Zut !"
            }
        };
    }
}
