﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HomeBackend.Models
{
    public class Card
    {
        public int Id { get; set; }
        public string Nom { get; set; }
        public List<Todo> todos { get; set; }
    }
}
