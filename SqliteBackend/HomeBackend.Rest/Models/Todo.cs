﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HomeBackend.Models
{
    public class Todo
    {
        public int Id { get; set; } 
        public int Ordre { get; set; }
        public int CardId { get; set; }
        public string Description { get; set; }
    }
}
